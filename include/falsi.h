#ifndef ZADANIE1_FALSI_H
#define ZADANIE1_FALSI_H

#include <iostream>
#include <vector>
#include <cmath>

double falsi(double (*f)(double), double a, double b, double epsilon, unsigned int maxIterations);

#endif //ZADANIE1_FALSI_H
