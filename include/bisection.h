#ifndef ZADANIE1_BISECTION_H
#define ZADANIE1_BISECTION_H

#include <iostream>
#include <vector>
#include <cmath>

double bisection(double (*f)(double), double a, double b, double epsilon, unsigned int maxIterations);

#endif //ZADANIE1_BISECTION_H
