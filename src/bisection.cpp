#include "bisection.h"

using namespace std;

double bisection(double (*f)(double), double a, double b, double epsilon, unsigned int maxIterations) {

    std::vector<double> xi;
    double x0;
    int iteration = 0;

    do {
        x0 = (a + b) / 2;
        xi.push_back(x0);

        if (f(x0) * f(a) < 0) {
            b = x0;
        } else if (f(x0) * f(b) < 0) {
            a = x0;
        }

        iteration++;
    } while ((iteration < maxIterations) && (fabs(xi.end()[-1] - xi.end()[-2]) > epsilon));

    cout << "\nBisekcja:\n";
    cout << "ilosc iteracji: " << iteration << endl;
    cout << "x0 = " << x0 << endl;
    cout << "|xi - xi-1| = " << fabs(xi.end()[-1] - xi.end()[-2]) << endl;
    cout << "f(x0) = " << f(x0) << endl;

    return x0;
}

