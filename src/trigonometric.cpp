#include "trigonometric.h"

double trigonometric(double x) {
    return 4 * sin(x) + 1;
}