#include <iostream>

#include "bisection.h"
#include "falsi.h"

#include "polynomial.h"
#include "trigonometric.h"
#include "exponential.h"
#include "composite.h"

#include "gnuplot_i.hpp"

#define GNUPLOT_PATH "C:\\gnuplot\\bin"

using namespace std;


int main() {
    int choice;
    double a, b;
    double epsilon = -1;
    unsigned int maxIterations = UINT_MAX;
    char t = 'n';
    double (*f)(double);
    int methodChoice;

    double (*functions[])(double) = {polynomial, trigonometric, exponential, composite};
    string functions_str[] = {"3x^3+2x^2-4x-1", "4sin(x)+1", "3e^(x-1)-3", "1-4sin(3-3e^(x-1))"};

    while (t == 'n') {

        system("cls");

        cout << "Wybierz funkcje:\n";
        cout << "[1] 3x^3+2x^2-4x-1\n";
        cout << "[2] 4sin(x)+1\n";
        cout << "[3] 3e^(x-1)-3\n";
        cout << "[4] 1-4sin(3-3e^(x-1))\n";
        cout << "Wybieram: ";
        cin >> choice;

        while (choice > 4 || choice < 1) {
            cout << "\nBledny wybor!\nWybieram: ";
            cin >> choice;
        }

        f = functions[choice - 1];

        cout << "\nWybierz warunek stopu:\n";
        cout << "[1] limit iteracji\n";
        cout << "[2] osiagniecie zadanej dokladnosci\n";
        cout << "Wybieram: ";
        cin >> methodChoice;
        while (methodChoice > 2 || methodChoice < 1) {
            cout << "\nBledny wybor!\nWybieram: ";
            cin >> methodChoice;
        }

        cout << "\nWprowadz zakres <a, b>\n";
        cout << "a: ";
        cin >> a;
        cout << "b: ";
        cin >> b;

        while (f(a) * f(b) >= 0) {
            cout << "\nZle. Wprowadz zakres <a, b>\n";
            cout << "a: ";
            cin >> a;
            cout << "b: ";
            cin >> b;
        }

        if (methodChoice == 1) {
            cout << "\nWprowadz maksymalna ilosc iteracji: ";
            cin >> maxIterations;
        }
        if (methodChoice == 2) {
            cout << "\nWprowadz dokladnosc: ";
            cin >> epsilon;
        }

        //Bisection
        double x0B = bisection(f, a, b, epsilon, maxIterations);

        //Reguła falsi
        double x0F = falsi(f, a, b, epsilon, maxIterations);

        //=====================================================================
        //                       Wykres
        //=====================================================================
        Gnuplot::set_GNUPlotPath(GNUPLOT_PATH);
        Gnuplot main_plot;

        // Podpisy na wykresie, zeby bylo wiadomo co na nim widac
        main_plot.set_xlabel("oś X");
        main_plot.set_ylabel("oś Y");

        // siatka poprawia czytelnosc
        main_plot.set_grid();

        // zakres osi x
        main_plot.set_xrange(a, b);

        // styl rysowania wykresu
        main_plot.set_style("lines");

        // dodanie wykresu
        vector<double> x{};
        vector<double> y{};

        for (double i = a; i < b; i += 0.1) {
            x.push_back(i);
            y.push_back(f(i));
        }

        main_plot.plot_xy(x, y, functions_str[choice - 1]);


        // dodanie punktu - pierwiastek (bisekcja)
        vector<double> point_x = {x0B};
        vector<double> point_y = {f(x0B)};
        main_plot.set_style("points");
        main_plot.set_pointsize(4.0);
        main_plot.plot_xy(point_x, point_y, "Bisekcja: pierwiastek");


        // dodanie punktu - pierwiastek (reguła falsi)
        point_x = {x0F};
        point_y = {f(x0F)};
        main_plot.set_style("points");
        main_plot.set_pointsize(4.0);
        main_plot.plot_xy(point_x, point_y, "Regula falsi: pierwiastek");


        cout << "\nKonczymy dzialanie programu? ";
        cout << "\n[y] - zakoncz program";
        cout << "\n[n] - rozpocznij program od nowa ";
        cout << "\n\nWybieram: ";
        cin >> t;
    }

    return 0;
}
