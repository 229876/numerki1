#include "polynomial.h"

double polynomial(double x) {

    int st = 3;

    double wsp[] = {3, 2, -4, -1};

    double wynik = wsp[0];

    for(int i=1; i<= st; i++)
        wynik = wynik*x + wsp[i];

    return wynik;
}
