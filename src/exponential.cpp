#include "exponential.h"

double exponential(double x) {
    return 3 * x * exp(x - 1) - 3;
}

