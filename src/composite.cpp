#include "composite.h"

double composite(double x) {
    double w = 1 - 4 * sin(3 - 3 * exp(x - 1));
    return w;
}
